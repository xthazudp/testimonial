import React from 'react';
import Testimonial from './Testimonial/TestimonialComponent';

const AppRouting = () => {
  return (
    <div>
      <Testimonial />
    </div>
  );
};

export default AppRouting;
