import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/AppComponent';

const container = document.getElementById('root');
ReactDOM.render(<App></App>, container);
